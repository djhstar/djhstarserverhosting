﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DJHSTAR_Server_Hosting
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "DJHSTAR", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "IndexPage",
            //    url: "djhstarserverhosting/azurewebsites.net",
            //    defaults: new { controller = "DJHSTARController", action = "Index", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "LocalHostIndexPage",
            //    url: "localhost:56578/",
            //    defaults: new { controller = "DJHSTARController", action = "Index", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "HomePage",
            //    url: "localhost:56578/Home",
            //    defaults: new { controller = "DJHSTAR", action = "Index", id = UrlParameter.Optional }
            //);

        }
    }
}
