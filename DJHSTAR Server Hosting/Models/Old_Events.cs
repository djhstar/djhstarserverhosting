﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DJHSTAR_Server_Hosting.Models
{
    public class Old_Events
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime DateOfEventCreation { get; set; }
        public int StatusId { get; set; }
    }
}