﻿using System;
using G = System.Configuration;
using D = System.Data;
using C = System.Data.SqlClient;
using T = System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DJHSTAR_Server_Hosting.Models
{
    public class Old_Status
    {
        public string Name { get; set; }
        public string ColourCode { get; set; }
        public string ColourName { get; set; }
        public Boolean Scheduled { get; set; }
        public Boolean Active { get; set; }
        public Boolean Resolved { get; set; }
    }

    public class StatusCodes
    {
        public static List<Old_Status> GetAllStatusCodes()
        {
            string sqlQuery = string.Format("select * from t_status");
            string connectionString = "Server=tcp:hup1goe7d4.database.windows.net,1433;Database=djhstar;User ID=djhstar@hup1goe7d4;Password=Harvey16;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            List<Old_Status> StatusCodes = new List<Old_Status>();

            using (C.SqlConnection connection = new C.SqlConnection(connectionString))
            {
            C.SqlCommand command = new C.SqlCommand();
            command.Connection = connection;
            command.CommandType = D.CommandType.Text;
            command.CommandText = @sqlQuery;

            connection.Open();

            C.SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Old_Status Status1 = new Old_Status();
                    Status1.Name = reader[1].ToString();
                    Status1.ColourCode = reader[2].ToString();
                    Status1.ColourName = reader[3].ToString();
                    Status1.Scheduled = Convert.ToBoolean(reader[4].ToString());
                    Status1.Active = Convert.ToBoolean(reader[5].ToString());
                    Status1.Resolved = Convert.ToBoolean(reader[6].ToString());
                    StatusCodes.Add(Status1);
                }
            }

            return StatusCodes;
        }
    }
}