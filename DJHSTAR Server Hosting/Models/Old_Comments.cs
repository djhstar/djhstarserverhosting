﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DJHSTAR_Server_Hosting.Models
{
    public class Old_Comments
    {
        public string Comment { get; set; }
        public DateTime DateOfComment { get; set; }
        public int EventId { get; set; }
        
    }
}