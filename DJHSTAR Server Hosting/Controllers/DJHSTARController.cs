﻿using DJHSTAR_Server_Hosting.Models;
using DJHSTAR_Server_Hosting.Services;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;
using System.Web.Mvc;
//using Newtonsoft.Json;
//using BBC_RSS_Feed_Application.Models;

namespace DJHSTAR_Server_Hosting.Controllers
{
    public class DJHSTARController : Controller
    {
        private djhstarEntities db = new djhstarEntities();

        public ActionResult Index()
        {
           //List<Old_Status> model = new List<Old_Status>();
           //model = StatusCodes.GetAllStatusCodes();
            var events = db.events.Include(e => e.t_status).Where(e => e.t_status.resolved == false);
            return View(events);
           //return View(model);
        }

        public ActionResult ResolvedEvents()
        {
            var events = db.events.Include(e => e.t_status).Where(e => e.t_status.resolved == true);
            return View(events);
        }

        public ActionResult EventService()
        {
            DJHSTAR_Server_Hosting.Services.Host.StartEventService();
            return this.RedirectToAction("Index");
            //var events = db.events.Include(e => e.t_status);
            //return View(events);
        }
    }
}