﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DJHSTAR_Server_Hosting.Models;

namespace DJHSTAR_Server_Hosting.Controllers
{
    public class eventsController : Controller
    {
        private djhstarEntities db = new djhstarEntities();

        // GET: events
        public ActionResult Index()
        {
            var events = db.events.Include(e => e.t_status);
            return View(events.ToList());
        }

        // GET: events/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            events events = db.events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        // GET: events/Create
        public ActionResult Create()
        {
            ViewBag.id_status = new SelectList(db.status, "id", "name");
            return View();
        }

        // POST: events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,title,content,dateOfEventCreation,id_status")] events events)
        {
            if (ModelState.IsValid)
            {
                events.updated = DateTime.Now;
                db.events.Add(events);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_status = new SelectList(db.status, "id", "name", events.id_status);
            return View(events);
        }

        // GET: events/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            events events = db.events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_status = new SelectList(db.status, "id", "name", events.id_status);
            return View(events);
        }

        // POST: events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,title,content,dateOfEventCreation,id_status")] events events)
        {
            if (ModelState.IsValid)
            {
                events.updated = DateTime.Now;
                db.Entry(events).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_status = new SelectList(db.status, "id", "name", events.id_status);
            return View(events);
        }

        // GET: events/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            events events = db.events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        // POST: events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            events events = db.events.Find(id);
            db.events.Remove(events);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
