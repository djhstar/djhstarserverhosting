﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DJHSTAR_Server_Hosting.Models;

namespace DJHSTAR_Server_Hosting.Controllers
{
    public class fixesController : Controller
    {
        private djhstarEntities db = new djhstarEntities();

        // GET: fixes
        public ActionResult Index()
        {
            var fixes = db.fixes.Include(f => f.t_events);
            return View(fixes.ToList());
        }

        // GET: fixes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            fix fix = db.fixes.Find(id);
            if (fix == null)
            {
                return HttpNotFound();
            }
            return View(fix);
        }

        // GET: fixes/Create
        public ActionResult Create()
        {
            ViewBag.id_event = new SelectList(db.events, "id", "title");
            return View();
        }

        // POST: fixes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,startDateTimeOfFix,endDateTimeOfFix,id_event")] fix fix)
        {
            if (ModelState.IsValid)
            {
                fix.t_events = db.events.Find(fix.id_event);
                fix.t_events.updated = DateTime.Now;
                db.Entry(fix.t_events).State = EntityState.Modified;
                db.fixes.Add(fix);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_event = new SelectList(db.events, "id", "title", fix.id_event);
            return View(fix);
        }

        // GET: fixes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            fix fix = db.fixes.Find(id);
            if (fix == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_event = new SelectList(db.events, "id", "title", fix.id_event);
            return View(fix);
        }

        // POST: fixes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,startDateTimeOfFix,endDateTimeOfFix,id_event")] fix fix)
        {
            if (ModelState.IsValid)
            {
                fix.t_events = db.events.Find(fix.id_event);
                fix.t_events.updated = DateTime.Now;
                db.Entry(fix.t_events).State = EntityState.Modified;
                db.Entry(fix).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_event = new SelectList(db.events, "id", "title", fix.id_event);
            return View(fix);
        }

        // GET: fixes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            fix fix = db.fixes.Find(id);
            if (fix == null)
            {
                return HttpNotFound();
            }
            return View(fix);
        }

        // POST: fixes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            fix fix = db.fixes.Find(id);
            db.fixes.Remove(fix);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
