﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DJHSTAR_Server_Hosting.Models;

namespace DJHSTAR_Server_Hosting.Controllers
{
    public class commentsController : Controller
    {
        private djhstarEntities db = new djhstarEntities();

        // GET: comments
        public ActionResult Index()
        {
            var comments = db.comments.Include(c => c.t_events);
            return View(comments.ToList());
        }

        // GET: comments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            comments comments = db.comments.Find(id);
            if (comments == null)
            {
                return HttpNotFound();
            }
            return View(comments);
        }

        // GET: comments/Create
        public ActionResult Create()
        {
            ViewBag.id_event = new SelectList(db.events, "id", "title");
            ViewBag.id_status = new SelectList(db.status, "id", "name");
            return View();
        }

        // POST: comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,comment,dateOfComment,id_event,id_status")] comments comments)
        {
            if (ModelState.IsValid)
            {
                comments.t_events = db.events.Find(comments.id_event);
                comments.t_events.updated = DateTime.Now;
                comments.t_events.id_status = comments.id_status;
                db.Entry(comments.t_events).State = EntityState.Modified;
                db.comments.Add(comments);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_event = new SelectList(db.events, "id", "title", comments.id_event);
            ViewBag.id_status = new SelectList(db.status, "id", "name", comments.t_events.id_status);
            return View(comments);
        }

        // GET: comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            comments comments = db.comments.Find(id);
            if (comments == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_event = new SelectList(db.events, "id", "title", comments.id_event);
            ViewBag.id_status = new SelectList(db.status, "id", "name", comments.t_events.id_status);
            return View(comments);
        }

        // POST: comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,comment,dateOfComment,id_event,id_status")] comments comments)
        {
            if (ModelState.IsValid)
            {
                comments.t_events = db.events.Find(comments.id_event);
                comments.t_events.updated = DateTime.Now;
                comments.t_events.id_status = comments.id_status;
                db.Entry(comments.t_events).State = EntityState.Modified;
                db.Entry(comments).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_event = new SelectList(db.events, "id", "title", comments.id_event);
            ViewBag.id_status = new SelectList(db.status, "id", "name", comments.t_events.id_status);
            return View(comments);
        }

        // GET: comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            comments comments = db.comments.Find(id);
            if (comments == null)
            {
                return HttpNotFound();
            }
            return View(comments);
        }

        // POST: comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            comments comments = db.comments.Find(id);
            db.comments.Remove(comments);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
