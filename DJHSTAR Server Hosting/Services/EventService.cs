﻿using System;
using System.Xml;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.ServiceModel.Description;
using System.ServiceModel.Syndication;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web.Mvc;
using System.Net;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Text.RegularExpressions;
using LinqToTwitter;
using DJHSTAR_Server_Hosting.Models;

namespace DJHSTAR_Server_Hosting.Services
{
    [ServiceContract]
    public interface IEventFeed
    {
        [OperationContract]
        [WebGet]
        Rss20FeedFormatter GetEvents();
    }

    public class RSSEventService : IEventFeed
    {

        private djhstarEntities db = new djhstarEntities();

        public Rss20FeedFormatter GetEvents()
        {
            SyndicationFeed feed = new SyndicationFeed("DJHSTAR Event Feed", "This is a test feed", new Uri("http://djhstarserverhosting.azurewebsites.net/djhstar/EventService"));
            //SyndicationFeed feed = new SyndicationFeed("DJHSTAR Event Feed", "This is a test feed", new Uri("http://localhost:56578/djhstar/eventservice"));
            feed.Authors.Add(new SyndicationPerson("d.henderson16@btinternet.com"));
            //feed.Categories.Add(new SyndicationCategory("Status"));
            feed.Description = new TextSyndicationContent("This feed displays DJHSTAR server hosting issues...");

            List<SyndicationItem> items = new List<SyndicationItem>();

            var events = db.events.Include(e => e.t_status).Where(e => e.t_status.resolved == false);

            foreach (var item in events)
            {
                SyndicationItem item1 = new SyndicationItem(
                    item.title + " - " + item.t_status.name,
                    item.content,
                    new Uri("http://djhstarserverhosting.azurewebsites.net"),
                    //new Uri("http://localhost:56578"),
                    item.id.ToString(),
                    DateTime.Now);

                items.Add(item1);
            }

            feed.Items = items;

            return new Rss20FeedFormatter(feed);
        }
    }

    public class Host
    {
        public static void StartEventService()
        {

            //Regex myRegex = new Regex(@"http://djhstarserverhosting.azurewebsites.net/djhstar/EventService");
            //WebPermission myWebPermission = new WebPermission(NetworkAccess.Connect, myRegex);
            //http://djhstarserverhosting.azurewebsites.net/djhstar/EventService


            Uri baseAddress = new Uri("http://djhstarserverhosting.azurewebsites.net/djhstar/EventService");
            //Uri baseAddress = new Uri("http://localhost:56578/djhstar/EventService");

            //ServiceHost svcHost = new ServiceHost(typeof(RSSEventService), baseAddress);
            WebServiceHost svcHost = new WebServiceHost(typeof(RSSEventService), baseAddress);
            //ServiceAuthorizationBehavior author = svcHost.Authorization;
            //ServiceAuthenticationBehavior authen = svcHost.Authentication;
            //WebBrowser webbrowser1 = new WebBrowser();
            //var uri = webbrowser1.Url.AbsoluteUri;
            //var auth = HttpBrowserCapabilities
            //.Source.ToString();
            //Debug.WriteLine(auth);
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            basicHttpBinding.Security.Mode = BasicHttpSecurityMode.None;
            basicHttpBinding.HostNameComparisonMode = HostNameComparisonMode.Exact;
            EndpointAddress endpointAddress = new EndpointAddress("http://djhstarserverhosting.azurewebsites.net/djhstar/EventService");
            ServiceEndpoint serviceEndpoint = 
                new ServiceEndpoint(ContractDescription.GetContract(typeof(IEventFeed)),
                    basicHttpBinding,
                    endpointAddress);
            svcHost.AddServiceEndpoint(serviceEndpoint);
            
           

            //try
            //{

            //svcHost.Close();

                
                svcHost.Open();
                Debug.WriteLine("Service is running");               
                
                //XmlReader reader = XmlReader.Create("http://localhost:56578/djhstar/eventservice/GetEvents");
                XmlReader reader = XmlReader.Create("http://djhstarserverhosting.azurewebsites.net/djhstar/EventService/GetEvents");
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                Debug.WriteLine(feed.Title.Text);
                Debug.WriteLine("Items:");
                foreach (SyndicationItem item in feed.Items)
                {
                    Debug.WriteLine(string.Format("Title: {0}", item.Title.Text));
                    Debug.WriteLine(string.Format("Summary: {0}", ((TextSyndicationContent)item.Summary).Text));

                }
                //Debug.WriteLine("RSS feed active events tweeted..");
                //Debug.WriteLine("Press <enter> to quit...");
                //Console.ReadLine();
                svcHost.Close();
                ActionTweets(feed);
            //}
            //catch (CommunicationException ce)
            //{
            //    Debug.WriteLine("An exception occurred: {0}", ce.Message);
            //    svcHost.Abort();
            //}
        }

        static async Task<Status> SendTweet(string tweet, TwitterContext twitterCtx)
        {
            var messageStatus = await twitterCtx.TweetAsync(tweet);
            return messageStatus;
        }

        public static void ActionTweets(SyndicationFeed feed)
        {
            SingleUserAuthorizer authorizer = new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    ConsumerKey = "YVC4kxy288oP5c7vFwDpGqdue",
                    ConsumerSecret = "4dFb3eUvfN1PnE52rUuYWmUgV2mFpLAXx4Lz22EDWir5CsK9Wu",
                    AccessToken = "94600847-i9yeTFvng9YhCjfwrmpuJ6JFsxtQK0uIn6i1HATRk",
                    AccessTokenSecret = "wNALwrKSOS4yO5ZwrL3U6XfPpvqcXNqljiyVDdGjaxFOM"
                }
            };

            TwitterContext twitterCtx = new TwitterContext(authorizer);

            //Debug.WriteLine(twitterCtx);

            var finalTweet = "temp";
            if (feed.Items.Count() == 0)
            {
                finalTweet = "DJHSTAR Server Hosting - No scheduled or active events... on " + DateTime.Now;
            }
            else
            {
                finalTweet = "All RSS feed active events tweeted.. on " + DateTime.Now;
            }
            foreach (SyndicationItem item in feed.Items)
            {

                var tweet = item.Title.Text + " " + ((TextSyndicationContent)item.Summary).Text + " " + DateTime.Now;
                if (tweet.Length < 140)
                {
                    //Debug.Write("about to tweet");
                    var result1 = Task.Run(() => SendTweet(tweet, twitterCtx));
                    result1.Wait();
                    //Debug.WriteLine("1st tweet tweeted");
                }
                else
                {
                    tweet = "Event text too long for a tweet...";
                    var result2 = Task.Run(() => SendTweet(tweet, twitterCtx));
                    result2.Wait();
                }
            }
            Debug.WriteLine("RSS feed active events tweeted..");
            var result3 = Task.Run(() => SendTweet(finalTweet, twitterCtx));
            result3.Wait();



        }
    }
}
