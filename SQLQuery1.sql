﻿if not exists ( select * from sys.columns where object_id = OBJECT_ID(N'[dbo].[t_events]') and name = 'updated')
	begin
	alter table dbo.t_events
	add updated datetime default NULL
end